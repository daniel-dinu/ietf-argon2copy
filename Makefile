FILE=draft-irtf-cfrg-argon2

XML_FILE=$(FILE).xml

TEXT_FILE=$(FILE).txt
HTML_FILE=$(FILE).html
NROFF_FILE=$(FILE).nroff
RAW_FILE=$(FILE).raw.txt
EXP_FILE=$(FILE).exp.xml


.PHONY: all create create-text create-html create-nroff create-raw create-exp \
		create-all clean

all: clean create

create: create-text

create-text:
	xml2rfc $(XML_FILE) --text

create-html:
	xml2rfc $(XML_FILE) --html

create-nroff:
	xml2rfc $(XML_FILE) --nroff

create-raw:
	xml2rfc $(XML_FILE) --raw

create-exp:
	xml2rfc $(XML_FILE) --exp

create-all:
	xml2rfc $(XML_FILE) --text --html --nroff --raw --exp

clean:
	rm -rf *~
	rm -rf $(TEXT_FILE)
	rm -rf $(HTML_FILE)
	rm -rf $(NROFF_FILE)
	rm -rf $(RAW_FILE)
	rm -rf $(EXP_FILE)

